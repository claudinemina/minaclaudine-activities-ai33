var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY'],
        borderColor : "#fffff",
        datasets: [{
            label: 'Borrowed Books, Returned Books',
            data: [6, 12, 8, 5, 2, 5, 4],
            borderColor : "#fff",
            hoverBorderColor : "#000",
            backgroundColor: [
                'rgb(0, 102, 0)',
                'rgb(255, 0, 102)',
                'rgb(0, 102, 0)',
                'rgb(255, 0, 102)',
                'rgb(0, 102, 0)',
                'rgb(255, 0, 102)',
                'rgb(0, 102, 0)'
               

            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['Computer Science', 'History', 'Chemistry', 'Biology'],
        datasets: [{
            label: '# of Votes',
            data: [10, 8, 7, 5],
            backgroundColor: [
                'rgb(255, 0, 0)',
                'rgb(255, 165, 0)',
                'rgb(51, 204, 255)',
                'rgb(0, 0, 255)'
                
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



